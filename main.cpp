#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<vector>
#define K 300
#define M 4
#include"matrix.h"
vector<double>& operator+=(vector<double>& v1, const vector<double>& v2) {
    for(int i=0;i<v1.size();i++){
        v1[i]+=v2[i];
    }
    return v1;
}
long clockdiff(timespec t1, timespec t2) {
    return (1000000000 * (t2.tv_sec - t1.tv_sec) + t2.tv_nsec - t1.tv_nsec) / 1000000;
}
vector<double> createVec(int size) {
    vector<double> v(size);
    for(int i=0;i<size;i++){
        v[i] = M+i;
    }
    return v;
}
int parse_argv(char* arg) {
    int res;
    sscanf(arg, "%d", &res);
    return res;
}
void test_blocksize(const matrix& A, const int& bs) {
    timespec t1, t2;
    vector<double> b(A.size(),0);
    fprintf(stderr, "block %d\n", bs);
    auto x = createVec(A.size());
    auto B = A.jacobize();
    B.setBlockSize(bs);
    A.multiplySingle(x,b);
    auto g = jacobizeVec(A,b);

    vector<double> xi(A.size(),0);
    xi[0] = 1;
    vector<double> xip(A.size(),0);

    clock_gettime(CLOCK_MONOTONIC, &t1);
    for(int i=0;i<K;i++) {
        xip.assign(g.begin(),g.end());
        B.multiply(xi,xip);
//        xip+=g;
        xip.swap(xi);
    }
    clock_gettime(CLOCK_MONOTONIC, &t2);
    printf("%ld\n", clockdiff(t1, t2));
}
int main(int argc, char* argv[]) {
    int N;
    srand(time(NULL));
    N = parse_argv(argv[1]);
    matrix a(N);
    a.lab3Randomize();
    if(argc > 2) {
        int bs = parse_argv(argv[2]);
        test_blocksize(a, bs);
    } else {
        vector<int> bsises {2, 5, 10, 16, 20, 32, 64, 128, 256, 512};
        for(auto i = bsises.begin(); i != bsises.end() && (*i)<N; i++) {
            test_blocksize(a, *i);
        }
    }
    return 0;
}
