#include<stdlib.h>
#include<memory>
#include<vector>
#include<iostream>
using namespace std;
class matrix {
private:
    double** data;
    int n;
    double* b;
    int bs;

    void multiplyBlock(int block,const vector<double>::const_iterator x0, vector<double>::iterator x1) const {
        //cerr << block << endl;
        int bNum = n/bs;
        int row = block / bNum;
        int col = block % bNum;
        int ioffset = row * bs;
        int joffset = col * bs;
        for(int i = 0; i < bs; i++)
            for(int j = 0; j < bs; j++)
                *(x1+i) += data[ioffset + i][joffset + j] * *(x0 + j);
    }
public:
    matrix(int _n): n(_n) {
        data = new double*[n];
        for(int i = 0; i < n; i++) {
            data[i] = new double[n];
        }
    }
    ~matrix() {
        for(int i = 0; i < n; i++) {
            delete[] data[i];
        }
        delete[] data;
    }
    int size() const {
        return n;
    }
    void setBlockSize(int bsize) {
        bs = bsize;
    }
    void lab3Randomize() {
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                data[i][j] = -rand() % 5;
            }
        }
        for(int i = 0; i < n; i++) {
            double sum = 0;
            data[i][i] = 0;
            for(int j = 0; j < n; j++) {
                sum += data[i][j];
            }
            data[i][i] = -sum;
        }
        data[0][0]++;
    }
    matrix jacobize() const {
        matrix J(size());
        for(int i=0;i<J.size();i++) {
            J.data[i][i] = 0;
        }
        for(int i=0;i<J.size();i++) {
            for(int j=0;j<J.size();j++) {
                J.data[i][j]=-data[i][j]/data[i][i];
            }
        }
        return J;
    }
    void multiply(const vector<double>& x0, vector<double>& x1) const {
        if (bs == 1) {
            multiplySingle(x0,x1);
            return;
        }
        int blockRow = (n/bs);
        int blockCount = blockRow * blockRow;
        #pragma omp parallel for
        for(int i=0;i<blockRow;i++) {
            for(int j=0;j<blockRow;j++) {
                multiplyBlock(i * blockRow + j,x0.begin() + j * bs, x1.begin() + i * bs);
            }
        }
    }
    void multiplySingle(const vector<double>& x0, vector<double>& x1) const {
        for(int i=0;i<n;i++) {
            for(int j=0;j<n;j++) {
                x1[i] += data[i][j] * x0[j];
            }
        }
    }
    friend vector<double> jacobizeVec(const matrix& A, const vector<double>& b);
};

vector<double> jacobizeVec(const matrix& A, const vector<double>& b) {
    vector<double> v(b);
    for(int i=0;i<v.size();i++) {
        v[i]/=A.data[i][i];
    }
    return v;
}
