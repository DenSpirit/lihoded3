CC=g++
CFLAGS=-g -std=c++14 -fopenmp
main: main.cpp matrix.h
	$(CC) $(CFLAGS) main.cpp -o main
clean:
	rm main -f
astyle: main.cpp matrix.h
	astyle -p -n main.cpp matrix.h
